use chechire::CacheTable;
use rand::prelude::*;
use rand_distr::{Distribution, WeightedError, WeightedIndex};
use std::thread;
use std::time::{Duration, Instant};

mod easy_cache;
use easy_cache::{CacheTableDeque, CacheTableVec};

trait Cache {
    fn get(&self, k: &usize) -> Option<&usize>;
    fn insert(&mut self, k: usize, v: usize) -> Option<usize>;
}

macro_rules! impl_cache {
    ($name: ident) => {
        impl Cache for $name<usize, usize> {
            fn get(&self, k: &usize) -> Option<&usize> {
                $name::get(self, k)
            }
            fn insert(&mut self, k: usize, v: usize) -> Option<usize> {
                $name::insert(self, k, v)
            }
        }
    };
}

impl_cache! {CacheTable}
impl_cache! {CacheTableVec}
impl_cache! {CacheTableDeque}

struct Subject<C> {
    cache: C,
    hits: u64,
}

impl<C> Subject<C>
where
    C: Cache,
{
    fn new(cache: C) -> Self {
        Self { cache, hits: 0 }
    }
    fn test(&mut self, data: usize) {
        match self.cache.get(&data) {
            Some(&data) => {
                self.hits += 1;
                data
            }
            None => {
                thread::sleep(Duration::from_micros(500));
                self.cache.insert(data, 0);
                0
            }
        };
    }
}

struct WorkLoad {
    weights: WeightedIndex<usize>,
}

impl WorkLoad {
    fn new(init: usize) -> Result<Self, WeightedError> {
        use std::iter::{repeat, successors};
        use std::num::NonZeroUsize;
        Ok(Self {
            weights: WeightedIndex::new(
                successors(Some(init), |w| {
                    NonZeroUsize::new(w * 95 / 100).map(NonZeroUsize::get)
                })
                .enumerate()
                .flat_map(|(i, n)| repeat(n).take(i)),
            )?,
        })
    }

    fn next<R: Rng>(&mut self, rng: &mut R) -> usize {
        self.weights.sample(rng)
    }
}

fn run_bench<C: Cache>(name: &str, s: &mut Subject<C>) {
    let count = 1_000_000;
    let mut w = WorkLoad::new(100_000_000_000).unwrap();
    let mut rng = thread_rng();
    println!("running {}", name);
    println!("  warming up cache");
    for _ in 0..32 * 1024 {
        s.test(w.next(&mut rng))
    }
    s.hits = 0;
    println!("  benchmarking");
    let timer = Instant::now();
    for _ in 0..count {
        s.test(w.next(&mut rng))
    }
    let elapsed = timer.elapsed();
    println!("  elapsed:  {}ms", elapsed.as_millis());
    println!("  hit rate: {}%", s.hits as f64 / count as f64 * 100.0);
}

pub fn chechire() {
    let mut subject = Subject::new(CacheTable::with_capacity(16 * 1024));
    run_bench("chechire", &mut subject)
}

pub fn easy_cache1() {
    let mut subject = Subject::new(CacheTableVec::with_capacity(16 * 1024));
    run_bench("easy_cache(vec)", &mut subject)
}

pub fn easy_cache2() {
    let mut subject = Subject::new(CacheTableDeque::with_capacity(16 * 1024));
    run_bench("easy_cache(deque)", &mut subject)
}

fn main() {
    chechire();
    easy_cache1();
    easy_cache2();
}
