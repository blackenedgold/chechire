use std::borrow::Borrow;
use std::cell::RefCell;
use std::collections::HashMap;
use std::collections::VecDeque;
use std::hash::Hash;

pub struct CacheTableVec<K, V> {
    capacity: usize,
    table: HashMap<K, V>,
    lru: RefCell<Vec<K>>,
}

impl<K, V> CacheTableVec<K, V> {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            capacity,
            table: HashMap::with_capacity(capacity),
            lru: RefCell::new(Vec::with_capacity(capacity)),
        }
    }
}

impl<K, V> CacheTableVec<K, V>
where
    K: Eq + Hash,
{
    pub fn get<Q: ?Sized>(&self, k: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        match self.table.get(k) {
            Some(v) => {
                let pos = self
                    .lru
                    .borrow()
                    .iter()
                    // finding from back because recently used items there
                    .rposition(|e| e.borrow() == k)
                    .unwrap();
                let k = self.lru.borrow_mut().remove(pos);
                self.lru.borrow_mut().push(k);
                Some(v)
            }
            None => None,
        }
    }

    fn remove_one(&mut self) -> Option<V> {
        debug_assert!(!self.lru.borrow().is_empty());
        let k = self.lru.borrow_mut().remove(0);
        self.table.remove(&k)
    }

    pub fn insert(&mut self, k: K, v: V) -> Option<V>
    where
        K: Clone,
    {
        let mut removed = None;
        if self.capacity <= self.table.len() {
            removed = self.remove_one();
        }
        let ret = self.table.insert(k.clone(), v);
        self.lru.borrow_mut().push(k);
        removed.or(ret)
    }
}

pub struct CacheTableDeque<K, V> {
    capacity: usize,
    table: HashMap<K, V>,
    lru: RefCell<VecDeque<K>>,
}

impl<K, V> CacheTableDeque<K, V> {
    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            capacity,
            table: HashMap::with_capacity(capacity),
            lru: RefCell::new(VecDeque::with_capacity(capacity)),
        }
    }
}

impl<K, V> CacheTableDeque<K, V>
where
    K: Eq + Hash,
{
    pub fn get<Q: ?Sized>(&self, k: &Q) -> Option<&V>
    where
        K: Borrow<Q>,
        Q: Hash + Eq,
    {
        match self.table.get(k) {
            Some(v) => {
                let pos = self
                    .lru
                    .borrow()
                    .iter()
                    // finding from back because recently used items there
                    .rposition(|e| e.borrow() == k)
                    .unwrap();
                let k = self.lru.borrow_mut().remove(pos).unwrap();
                self.lru.borrow_mut().push_back(k);
                Some(v)
            }
            None => None,
        }
    }

    fn remove_one(&mut self) -> Option<V> {
        debug_assert!(!self.lru.borrow().is_empty());
        let k = self.lru.borrow_mut().pop_front().unwrap();
        self.table.remove(&k)
    }

    pub fn insert(&mut self, k: K, v: V) -> Option<V>
    where
        K: Clone,
    {
        let mut removed = None;
        if self.capacity <= self.table.len() {
            removed = self.remove_one();
        }
        let ret = self.table.insert(k.clone(), v);
        self.lru.borrow_mut().push_back(k);
        removed.or(ret)
    }
}
