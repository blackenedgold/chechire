use crate::policy::LeastRecentlyUsed as LRU;
use hashbrown::raw::{Bucket as RawBucket, RawDrain, RawIntoIter, RawIter, RawTable};
use std::cell::RefCell;
use std::iter::repeat_with;

pub struct Block<T> {
    pub(crate) table: RawTable<T>,
    policy: RefCell<LRU>,
}

impl<T> Block<T> {
    fn new() -> Self {
        Self::default()
    }
}

impl<T> Default for Block<T> {
    fn default() -> Self {
        // hashbrown uses 85% of buckets as capacity
        let table = RawTable::with_capacity(LRU::SIZE * 85 / 100);
        debug_assert_eq!(table.buckets(), LRU::SIZE);
        Self {
            table,
            policy: Default::default(),
        }
    }
}

pub struct RawCache<T> {
    pub(crate) blocks: Vec<Block<T>>,
}

pub struct Bucket<T> {
    pub(crate) bucket: RawBucket<T>,
    pub(crate) block: usize,
}

impl<T> Clone for Bucket<T> {
    fn clone(&self) -> Self {
        Self {
            bucket: self.bucket.clone(),
            block: self.block,
        }
    }
}

// TODO: let it configurable

impl<T> RawCache<T> {
    pub fn with_capacity(capacity: usize) -> Self {
        let num_blocks = capacity / LRU::SIZE;
        let blocks = repeat_with(Block::new).take(num_blocks).collect();
        Self { blocks }
    }

    fn block_mut(&mut self, hash: u64) -> &mut Block<T> {
        let index = self.block_index(hash);
        self.blocks.get_mut(index).unwrap()
    }

    fn block(&self, hash: u64) -> &Block<T> {
        let index = self.block_index(hash);
        self.blocks.get(index).unwrap()
    }

    fn block_index(&self, hash: u64) -> usize {
        let num_blocks = self.blocks.len();
        ((hash as usize) / LRU::SIZE) % num_blocks
    }

    fn block_hash(&self, hash: u64) -> u64 {
        hash
    }
    // FIXME: delete from LRU too
    pub unsafe fn erase(&mut self, item: Bucket<T>) {
        self.blocks
            .get_mut(item.block)
            .unwrap()
            .table
            .erase(item.bucket)
    }

    // FIXME: delete from LRU too
    pub fn erase_entry(&mut self, hash: u64, eq: impl FnMut(&T) -> bool) -> bool {
        let cache_hash = self.block_hash(hash);
        self.block_mut(hash).table.erase_entry(cache_hash, eq)
    }

    // FIXME: delete from LRU too
    pub unsafe fn remove(&mut self, item: Bucket<T>) -> T {
        self.blocks
            .get_mut(item.block)
            .unwrap()
            .table
            .remove(item.bucket)
    }

    // FIXME: delete from LRU too
    pub fn remove_entry(&mut self, hash: u64, eq: impl FnMut(&T) -> bool) -> Option<T> {
        let cache_hash = self.block_hash(hash);
        let block = self.block_mut(hash);
        block.table.remove_entry(cache_hash, eq)
    }

    pub fn clear_no_drop(&mut self) {
        for block in &mut self.blocks {
            block.table.clear_no_drop()
        }
    }

    pub fn clear(&mut self) {
        for block in &mut self.blocks {
            block.table.clear()
        }
    }

    pub fn insert(&mut self, hash: u64, value: T) -> Bucket<T> {
        let block_index = self.block_index(hash);
        let cache_hash = self.block_hash(hash);
        let block = self.block_mut(hash);
        unsafe {
            if (block.table.capacity() - block.table.len()) == 0 {
                let least = block.policy.borrow().least();
                let least_bucket = block.table.bucket(least);
                block.table.erase(least_bucket);
                block.policy.borrow_mut().delete(least);
            }
            debug_assert!(0 < block.table.capacity() - block.table.len());
            let bucket = block.table.insert_no_grow(cache_hash, value);
            let bucket_index = block.table.bucket_index(&bucket);
            block.policy.borrow_mut().refer(bucket_index);
            Bucket {
                bucket,
                block: block_index,
            }
        }
    }

    pub fn insert_entry(&mut self, hash: u64, value: T) -> &mut T {
        unsafe { self.insert(hash, value).bucket.as_mut() }
    }

    pub unsafe fn replace_bucket_with<F>(&mut self, bucket: Bucket<T>, f: F) -> bool
    where
        F: FnOnce(T) -> Option<T>,
    {
        self.blocks
            .get_mut(bucket.block)
            .unwrap()
            .table
            .replace_bucket_with(bucket.bucket, f)
    }

    pub fn find(&self, hash: u64, eq: impl FnMut(&T) -> bool) -> Option<Bucket<T>> {
        let block_index = self.block_index(hash);
        let cache_hash = self.block_hash(hash);
        let block = self.block(hash);
        unsafe {
            if let Some(bucket) = block.table.find(cache_hash, eq) {
                let bucket_index = block.table.bucket_index(&bucket);
                debug_assert!(bucket_index < LRU::SIZE);
                block.policy.borrow_mut().refer(bucket_index);
                Some(Bucket {
                    bucket,
                    block: block_index,
                })
            } else {
                None
            }
        }
    }

    pub fn get(&self, hash: u64, eq: impl FnMut(&T) -> bool) -> Option<&T> {
        match self.find(hash, eq) {
            Some(bucket) => Some(unsafe { bucket.bucket.as_ref() }),
            None => None,
        }
    }

    pub fn get_mut(&mut self, hash: u64, eq: impl FnMut(&T) -> bool) -> Option<&mut T> {
        match self.find(hash, eq) {
            Some(bucket) => Some(unsafe { bucket.bucket.as_mut() }),
            None => None,
        }
    }

    pub fn capacity(&self) -> usize {
        self.blocks.iter().map(|b| b.table.capacity()).sum()
    }

    pub fn len(&self) -> usize {
        self.blocks.iter().map(|b| b.table.len()).sum()
    }

    // pub fn buckets(&self) -> usize {
    //     self.blocks.iter().map(|b| b.table.len()).sum()
    // }

    pub unsafe fn iter(&self) -> Iter<T> {
        let vec = self
            .blocks
            .iter()
            .map(|block| block.table.iter())
            .collect::<Vec<_>>();
        vec.into_iter().flatten()
    }

    pub fn drain<'a>(&'a mut self) -> Drain<'a, T> {
        let f: fn(&mut Block<T>) -> RawDrain<T> = |block| unsafe {
            let iter = block.table.iter();
            block.table.drain_iter_from(iter)
        };
        self.blocks.iter_mut().map(f).flatten()
    }

    pub fn into_iter(self) -> IntoIter<T> {
        let f: fn(Block<T>) -> RawIntoIter<T> = |block| unsafe {
            let iter = block.table.iter();
            block.table.into_iter_from(iter)
        };
        self.blocks.into_iter().map(f).flatten()
    }
}

pub type Iter<T> = std::iter::Flatten<std::vec::IntoIter<RawIter<T>>>;

pub type IntoIter<T> = std::iter::Flatten<
    std::iter::Map<std::vec::IntoIter<Block<T>>, fn(Block<T>) -> RawIntoIter<T>>,
>;

pub type Drain<'a, T> = std::iter::Flatten<
    std::iter::Map<std::slice::IterMut<'a, Block<T>>, fn(&mut Block<T>) -> RawDrain<T>>,
>;
