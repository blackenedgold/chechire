#[derive(Debug, Clone, Default, Eq, PartialEq, Hash)]
pub(crate) struct LeastRecentlyUsed {
    bit_table: [u16; 16],
}

impl LeastRecentlyUsed {
    pub const SIZE: usize = 16;

    pub fn new() -> Self {
        Self::default()
    }

    pub fn refer(&mut self, index: usize) {
        debug_assert!(index < Self::SIZE);
        let mask = !(1 << index);
        for b in &mut self.bit_table {
            *b &= mask;
        }
        self.bit_table[index] = !0;
    }

    pub fn delete(&mut self, index: usize) {
        debug_assert!(index < Self::SIZE);
        self.bit_table[index] = 0;
    }

    pub fn least(&self) -> usize {
        let mut min = u16::MAX;
        let mut min_index = 0;
        for (i, &b) in self.bit_table.iter().enumerate() {
            // ignoring b == 0 because hashbrown assumes there is at least 1 blank bucket
            // thus there is at least 1 unavailable bucket.
            if 0 < b && b < min {
                min = b;
                min_index = i;
            }
        }
        min_index
    }
}

#[test]
fn test_used_exactly_one() {
    type LRU = LeastRecentlyUsed;
    let mut lru = LRU::new();
    for i in 0..LRU::SIZE {
        lru.refer(i)
    }
    assert_eq!(lru.least(), 0)
}

#[test]
fn test_used_exactly_twice() {
    type LRU = LeastRecentlyUsed;
    let mut lru = LRU::new();
    for i in 0..LRU::SIZE {
        lru.refer(i)
    }
    for i in 0..LRU::SIZE {
        lru.refer(LRU::SIZE - i - 1)
    }
    assert_eq!(lru.least(), 15)
}

#[test]
fn test_used_one_or_zero() {
    type LRU = LeastRecentlyUsed;
    let mut lru = LRU::new();
    for i in (0..LRU::SIZE).step_by(2) {
        lru.refer(i)
    }
    assert_eq!(lru.least(), 1)
}

#[test]
fn test_used_randomly() {
    type LRU = LeastRecentlyUsed;
    let mut lru = LRU::new();
    for i in 0..LRU::SIZE {
        lru.refer(i)
    }
    lru.refer(9);
    lru.refer(0);
    lru.refer(3);
    lru.refer(3);
    lru.refer(1);
    assert_eq!(lru.least(), 2)
}
